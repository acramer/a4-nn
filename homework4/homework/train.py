import torch
import torchvision
import numpy as np

from .models import Detector, save_model
from .utils import load_detection_data
from . import dense_transforms
import torch.utils.tensorboard as tb
from os import walk

class focal_loss(torch.nn.Module):
    def __init__(self, gamma=1.5, alpha=None):
        super().__init__()
        self.gamma = gamma
        self.alpha = alpha

    def forward(self, y):
        ip = 1 - torch.exp(-1*y)
        loss = ip.pow(self.gamma)*y
        if self.alpha is not None:
            for i, a in enumerate(self.alpha):
                loss[:,i,:,:] *= a
        return loss

def train(args):
    from os import path
    train_logger = None

    # Checkpoints
    # $PARSE_ARG
    best_valid_accuracy = 0

    # Determine Folder Number
    folder_nums = [-1]
    if args.log_dir is not None:
        # Find and sort Folder Numbers
        folder_nums = list(map(int, list(map(lambda x: x.split('-')[0], list(walk(args.log_dir))[0][1]))))
        folder_nums.sort()
        folder_nums.insert(0, -1)
        #Set Description with number and with or without set description
        if args.description != '': description = '/' + str(folder_nums[len(folder_nums)-1] + 1) + '-' + args.description
        else: description = '/' + str(folder_nums[-1] + 1)
        # Set up SummaryWriters
        train_logger = tb.SummaryWriter(path.join((args.log_dir + description), 'train'), flush_secs=1)

    # Find device and set model to device
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model = Detector().to(device)

    # Continuing training
    if args.continue_training:
        model.load_state_dict(torch.load(path.join(path.dirname(path.abspath(__file__)), 'det.th')))

    # Optimizer
    if args.adam:
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate, weight_decay=1e-5)
    else:
        optimizer = torch.optim.SGD(model.parameters(), lr=args.learning_rate, momentum=0.9, weight_decay=1e-3)

    # Step Scheduler
    if args.step_schedule:
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', patience=10)

    # Criterion
    if args.focal:
        floss = focal_loss()
        loss = torch.nn.BCEWithLogitsLoss(reduction='none') #.to(device)
    else:
        loss = torch.nn.BCEWithLogitsLoss(reduction='none') #.to(device)
    area_loss = torch.nn.MSELoss()

    # Data Loader and Augmentations
    transform = dense_transforms.Compose([
        dense_transforms.ToTensor(),
        dense_transforms.ToHeatmap(),
    ])
    train_data = load_detection_data('dense_data/train', batch_size=args.batch_size, transform=transform)
    # valid_data = load_detection_data('dense_data/valid', batch_size=args.batch_size, transform=transform)

    global_step = 0
    # Epoch Loop
    for i in range(args.epochs):
        model.train()
        total_loss = 0
        # Training Loop
        for img, label, area_label in train_data:
            # Set X and Y to Device
            img, label, area_label = img.to(device), label.to(device), area_label.to(device)

            # Compute logit and loss of peak
            logit = model(img)
            area_logit = logit[:,3:,:,:]
            loss_val = loss(logit[:,:3,:,:], label)
            if args.focal: loss_val = floss(loss_val)
            loss_val = torch.mean(loss_val)

            # Compute logit and loss of area
            area_loss_val = area_loss(area_logit, area_label)

            # Plot loss
            if train_logger is not None:
                train_logger.add_scalar('loss', loss_val, global_step)
                train_logger.add_scalar('area_loss', area_loss_val, global_step)

            # Grad Step
            optimizer.zero_grad()
            loss_val.backward(retain_graph=True)
            area_loss_val.backward()
            optimizer.step()
            global_step += 1
            total_loss += loss_val.item() + area_loss_val.item()

        # Display Image
        if train_logger is not None and i % 5 == 0:
            heatmap_prob = torch.sigmoid(logit[:,:3,:,:])
            image = torch.cat([heatmap_prob, label], 3).detach().cpu()
            train_logger.add_image('image', (torchvision.utils.make_grid(image, padding=5, pad_value=1) * 255).byte())

        # Print if no logger
        if train_logger is None:
            print("Epoch: {:4d} -- Training Loss: {:10.6f}".format(i, loss_val.item()))

        # Step Learning Rate
        if args.step_schedule:
            scheduler.step(total_loss)
            train_logger.add_scalar('learning rate', optimizer.param_groups[0]['lr'], global_step)

    # Training Complete Bookend
    print_args(args)
    print("Saved: ", not args.no_save)
    save_model(model)

def print_args(args):
    print("\t-----------------------------------------------------------------------------")
    print("\t|  Option Descriptions  |       Option Strings       |     Default Values    ")
    print("\t-----------------------------------------------------------------------------")
    print("\t|Epochs:                | '-e', '--epochs'           |  ", args.epochs)
    print("\t|Learning Rate:         | '-l', '--learning_rate'    |  ", args.learning_rate)
    print("\t|Batch Size:            | '-b', '--batch_size'       |  ", args.batch_size)
    # print("\t|Peak Theshold:         | '-t', '--theshold'         |  ", args.threshold)
    print("\t|Step Schedule:         | '--step_schedule'          |  ", args.step_schedule)
    print("\t|Description:           | '--description'            |  ", args.description)
    print("\t|No Save:               | '--no_save'                |  ", args.no_save)
    print("\t|Continue Training:     | '--continue_training'      |  ", args.continue_training)
    print("\t|Checkpoints:           | '--checkpoints'            |  ", args.checkpoints)
    # print("\t|Data Augmentation:     | '--data_aug'               |  ", args.data_aug)
    print("\t|Adam Optimizer:        | '--adam'                   |  ", args.adam)
    print("\t|Focal Loss:            | '--focal'                  |  ", args.focal)
    print("\t-----------------------------------------------------------------------------")

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--log_dir')
    parser.add_argument('-e', '--epochs', type=int, default=600)
    parser.add_argument('-l', '--learning_rate', type=float, default=1e-3)
    parser.add_argument('-b', '--batch_size', type=int, default=200)
    # parser.add_argument('-t', '--threshold', type=float, default=-0.2)
    parser.add_argument('--step_schedule', action='store_true', default=False)
    parser.add_argument('--description', type=str, default='')
    parser.add_argument('--no_save', action='store_true', default=False)
    parser.add_argument('--continue_training', action='store_true', default=False)
    parser.add_argument('--checkpoints', action='store_true', default=False)
    # parser.add_argument('--data_aug', action='store_true', default=False)
    parser.add_argument('--adam', action='store_true', default=False)
    parser.add_argument('--focal', action='store_true', default=False)
    parser.add_argument('-h', '--help', action='store_true', default=False)

    args = parser.parse_args()

    if args.help:
        print_args(args)
    else:
        train(args)
